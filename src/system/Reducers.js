import { combineReducers } from 'redux'
import { connectRouter } from 'connected-react-router'

import UserReducer from '../reducers/UserReducer'
import ItemsReducer from '../reducers/ItemsReducer'

const reducers = history =>
    combineReducers({
        user: UserReducer,
        items: ItemsReducer,
        router: connectRouter(history),
    })

export default reducers
