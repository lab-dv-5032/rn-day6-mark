import React from 'react'
import styled from 'styled-components'
import { Route, Switch } from 'react-router-native'

import {
    ROUTE_HOME,
    ROUTE_USER,
    ROUTE_USER_EDIT,
    ROUTE_ITEMS,
    ROUTE_ITEM,
    ROUTE_ITEM_CREATE,
    ROUTE_ITEM_EDIT,
} from '../constants/RouteConstants'
import {
    LoginPage,
    UserPage,
    UserEditPage,
    ItemsPage,
    ItemPage,
    ItemEditPage,
    ItemCreatePage,
} from '../containers'

export default class Router extends React.Component {
    render() {
        return (
            <DefaultBackground>
                <Switch>
                    <Route exact path={ROUTE_HOME} component={LoginPage} />
                    <Route exact path={ROUTE_USER} component={UserPage} />
                    <Route
                        exact
                        path={ROUTE_USER_EDIT}
                        component={UserEditPage}
                    />
                    <Route exact path={ROUTE_ITEMS} component={ItemsPage} />
                    <Route exact path={ROUTE_ITEM} component={ItemPage} />
                    <Route
                        exact
                        path={ROUTE_ITEM_EDIT}
                        component={ItemEditPage}
                    />
                    <Route
                        exact
                        path={ROUTE_ITEM_CREATE}
                        component={ItemCreatePage}
                    />
                </Switch>
            </DefaultBackground>
        )
    }
}

const DefaultBackground = styled.SafeAreaView`
    background-color: white;
    flex: 1;
    flex-direction: column;
`
