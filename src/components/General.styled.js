import styled from 'styled-components'

export const Body = styled.View`
    flex: 1;
    flex-direction: column;
`

export const Center = styled.View`
    align-items: center;
    justify-content: center;
`

export const Content = styled(Body)`
    padding-top: 8;
    padding-left: 16;
    padding-right: 16;
`

// Logo
export const LogoContainer = styled(Body)`
    justify-content: center;
    align-items: center;
`
export const Logo = styled.Image`
    width: 150;
    height: 150;
`

// Input
export const DefaultInput = styled.TextInput`
    padding-top: 8;
    padding-bottom: 8;
    padding-left: 8;
    padding-right: 8;
    margin-bottom: 8;
    width: 100%;
    border-radius: 8;
    border-width: 1;
    border-style: solid;
    border-color: #540d6e;
`

export const Button = styled.TouchableOpacity`
    padding-top: 8;
    padding-bottom: 8;
    padding-left: 8;
    padding-right: 8;
    margin-bottom: 8;
    width: 100%;
    border-radius: 8;
    background-color: #540d6e;
`
export const DefaultText = styled.Text`
    color: white;
`

// Header

export const HeaderContainer = styled.View`
    padding-top: 8;
    padding-bottom: 8;
    background-color: #540d6e;
    flex-direction: row;
    align-items: center;
`
export const HeaderIcon = styled.TouchableOpacity`
    padding-left: 8;
    padding-right: 8;
`
export const HeaderText = styled.Text`
    color: white;
    font-size: 24;
`

// content
export const Bold = styled.Text`
    font-weight: bold;
`
export const Field = styled.Text`
    padding-top: 8;
    padding-bottom: 8;
`
