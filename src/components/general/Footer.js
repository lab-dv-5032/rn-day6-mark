import React from 'react'
import { connect } from 'react-redux'
import { push } from 'connected-react-router'
import { TouchableOpacity } from 'react-native'
import { Icon } from '@ant-design/react-native'

import {
    HeaderContainer,
    HeaderText,
    HeaderIcon,
    Body,
    Center,
} from '../General.styled'

class Footer extends React.Component {
    render() {
        const { goToList, goToUser, goToAddItem } = this.props
        return (
            <HeaderContainer>
                <HeaderIcon onPress={goToList}>
                    <Icon name="shopping-cart" />
                </HeaderIcon>
                <Body>
                    <Center>
                        <TouchableOpacity onPress={goToAddItem}>
                            <HeaderText>Add Product</HeaderText>
                        </TouchableOpacity>
                    </Center>
                </Body>
                <HeaderIcon onPress={goToUser}>
                    <Icon name="user" />
                </HeaderIcon>
            </HeaderContainer>
        )
    }
}

export default connect(
    null,
    dispatch => ({
        goToList: state => dispatch(push('/products')),
        goToUser: state => dispatch(push('/user')),
        goToAddItem: state => dispatch(push('/product/add')),
    })
)(Footer)
