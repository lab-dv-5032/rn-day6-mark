import React from 'react'
import styled from 'styled-components'
import { FlatList } from 'react-native'

import Container from '../components/general/Container'
import ListItem from '../components/items/ListItem'

class ItemsPage extends React.Component {
    goToItem = state => {
        const { goToItem } = this.props
        goToItem(state)
    }
    renderItem = ({ item, index }) => {
        return (
            <ListItem
                {...item}
                onPress={() => this.goToItem({ ...item, index })}
            />
        )
    }
    render() {
        const { items } = this.props
        return (
            <Container hideGoBack={true}>
                <Wrapper>
                    <FlatList
                        numColumns={2}
                        data={items}
                        renderItem={this.renderItem}
                    />
                </Wrapper>
            </Container>
        )
    }
}

// HoC components
const Wrapper = styled.View`
    margin-left: -16;
    margin-right: -16;
`

export default ItemsPage
