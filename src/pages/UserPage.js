import React from 'react'
import styled from 'styled-components'

import Container from '../components/general/Container'
import {
    Body,
    Button,
    DefaultText,
    Bold,
    Field,
} from '../components/General.styled'
class UserPage extends React.Component {
    render() {
        const { username, firstname, lastname, goToEdit } = this.props
        return (
            <Container
                title={firstname || 'Profile'}
                hideFooter={true}
                hideGoBack={false}
            >
                <Body>
                    <Field>
                        <Bold>Username:</Bold> {username}
                    </Field>
                    <Field>
                        <Bold>Firstname:</Bold> {firstname}
                    </Field>
                    <Field>
                        <Bold>Lastname:</Bold> {lastname}
                    </Field>
                </Body>
                <EditButton onPress={goToEdit}>
                    <DefaultText>Edit</DefaultText>
                </EditButton>
            </Container>
        )
    }
}

// HoC components

const EditButton = styled(Button)`
    justify-content: center;
    align-items: center;
`

export default UserPage
