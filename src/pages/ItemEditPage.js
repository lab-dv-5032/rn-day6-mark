import React from 'react'
import styled from 'styled-components'
import { connect } from 'react-redux'
import { goBack } from 'connected-react-router'

import Container from '../components/general/Container'
import {
    Body,
    Button,
    DefaultText,
    DefaultInput,
} from '../components/General.styled'

class ItemEditPage extends React.Component {
    state = {
        index: null,
        image: '',
        name: '',
    }
    componentDidMount() {
        this.getDefaultValue()
    }
    getDefaultValue = () => {
        const { item } = this.props
        this.setState({ ...item })
    }
    onChangeValue = (index, value) => this.setState({ [index]: value })
    onSubmit = () => {
        const { onSave } = this.props
        const { index, image, name } = this.state
        onSave(index, { image, name })
    }
    render() {
        const { image, name } = this.state
        return (
            <Container
                title={'Add Product'}
                hideFooter={true}
                hideGoBack={false}
            >
                <Body>
                    <DefaultInput
                        placeholder={'Image'}
                        value={image}
                        onChangeText={value =>
                            this.onChangeValue('image', value)
                        }
                    />
                    <DefaultInput
                        placeholder={'Name'}
                        value={name}
                        onChangeText={value =>
                            this.onChangeValue('name', value)
                        }
                    />
                </Body>
                <SaveButton onPress={this.onSubmit}>
                    <DefaultText>Save</DefaultText>
                </SaveButton>
            </Container>
        )
    }
}

const SaveButton = styled(Button)`
    justify-content: center;
    align-items: center;
`

export default ItemEditPage
