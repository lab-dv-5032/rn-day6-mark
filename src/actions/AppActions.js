import {
    USER_SAVE,
    ITEM_ADD,
    ITEM_EDIT,
    ITEM_REMOVE,
} from '../constants/AppConstants'

// User Actions
export const userSave = ({ username, password, firstname, lastname }) => ({
    type: USER_SAVE,
    payload: { username, password, firstname, lastname },
})

// Items Actions
export const itemAdd = ({ image, name }) => ({
    type: ITEM_ADD,
    payload: { image, name },
})
export const itemEdit = (index, { image, name }) => ({
    type: ITEM_EDIT,
    payload: { index, image, name },
})
export const itemRemove = index => ({
    type: ITEM_REMOVE,
    payload: { index },
})
