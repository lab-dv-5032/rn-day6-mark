import { connect } from 'react-redux'
import { push } from 'connected-react-router'

import { ROUTE_USER_EDIT } from '../constants/RouteConstants'

import UserPage from '../pages/UserPage'

const mapStateToProps = ({ user }) => ({
    ...user,
})
const mapDispatchToProps = dispatch => ({
    goToEdit: () => {
        dispatch(push(ROUTE_USER_EDIT))
    },
})

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(UserPage)
