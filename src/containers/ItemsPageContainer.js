import { connect } from 'react-redux'
import { push } from 'connected-react-router'

import { ROUTE_ITEM } from '../constants/RouteConstants'

import ItemsPage from '../pages/ItemsPage'

const mapStateToProps = ({ items }) => ({
    items,
})
const mapDispatchToProps = dispatch => ({
    goToItem: item => {
        dispatch(push(ROUTE_ITEM, { ...item }))
    },
})

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(ItemsPage)
